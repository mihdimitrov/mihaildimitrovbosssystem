﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MihailDimitrovBossSystem.Startup))]
namespace MihailDimitrovBossSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
